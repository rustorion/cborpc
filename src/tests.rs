use crate as cborpc;
use anyhow::Result;

struct Adder {
	acc: u8,
}

impl cborpc::Protocol for Adder {
	type State = u8;

	fn name(&self) -> String {
		"adder".into()
	}
	fn method(&mut self, state: &mut Self::State, method_name: &str, _message: &[u8]) -> Result<cborpc::CallResponse> {
		match method_name {
			"add" => {
				self.acc += 1;
				*state = self.acc + 1;
				Ok(cborpc::CallResponse {
					success: true,
					message: [self.acc, *state].into(),
				})
			}
			_ => self.method_missing(method_name),
		}
	}
}

#[tokio::test]
async fn answering() {
	let value = 66;
	let (client, server) = tokio::io::duplex(666);
	let (client_r, client_w) = tokio::io::split(client);
	let (server_r, server_w) = tokio::io::split(server);
	let mut r = cborpc::Responder::new(value, server_r, server_w, None);
	r.add_protocol(Adder { acc: value });
	let mcall = cborpc::MethodCall {
		protocol_name: "adder".to_string(),
		method_name: "add".to_string(),
		message: vec![],
	};
	let mut caller = cborpc::Client::new(client_r, client_w, None);
	let answer_r = tokio::spawn(async move { caller.call(&mcall).await });
	let result_r = tokio::spawn(async move {
		r.answer_call().await?;
		Ok(r.state)
	});
	let answer = answer_r.await.unwrap().unwrap();
	let r: Result<u8> = result_r.await.unwrap();
	assert_eq!(answer.success, true);
	assert!(r.is_ok_and(|v| v == 68));
	assert_eq!(answer.message, vec![67, 68]);
}
