#[cfg(test)]
mod tests;

use anyhow::Result;
use anyhow::{anyhow, Context};
use futures_util::sink::SinkExt;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use thiserror::Error;
use tokio::io::{AsyncRead, AsyncWrite};
use tokio_stream::StreamExt;

pub struct FramedDuplex<ReadChannel: AsyncRead + Unpin, WriteChannel: AsyncWrite + Unpin> {
	framed_reader: tokio_util::codec::FramedRead<ReadChannel, tokio_util::codec::length_delimited::LengthDelimitedCodec>,
	framed_writer: tokio_util::codec::FramedWrite<WriteChannel, tokio_util::codec::length_delimited::LengthDelimitedCodec>,
}

impl<ReadChannel: AsyncRead + Unpin, WriteChannel: AsyncWrite + Unpin> FramedDuplex<ReadChannel, WriteChannel> {
	fn new(read_channel: ReadChannel, write_channel: WriteChannel, codec: Option<&tokio_util::codec::LengthDelimitedCodec>) -> Self {
		let codec = codec
			.cloned()
			.unwrap_or_else(|| tokio_util::codec::LengthDelimitedCodec::builder().max_frame_length(1_000_000_000).new_codec());
		FramedDuplex {
			framed_reader: tokio_util::codec::FramedRead::new(read_channel, codec.clone()),
			framed_writer: tokio_util::codec::FramedWrite::new(write_channel, codec.clone()),
		}
	}
}

pub struct Responder<State, ReadChannel: AsyncRead + Unpin, WriteChannel: AsyncWrite + Unpin> {
	state: State,
	protocols: HashMap<String, Box<dyn Protocol<State = State>>>,
	framed_duplex: FramedDuplex<ReadChannel, WriteChannel>,
}

pub trait Protocol: Send {
	type State;
	fn name(&self) -> String;
	fn method(&mut self, state: &mut Self::State, method_name: &str, message: &[u8]) -> Result<CallResponse>;
	/// shortcut for a catchall
	fn method_missing(&self, method_name: &str) -> Result<CallResponse> {
		let protocol_name = self.name();
		Err(MethodMissing {
			method_name: method_name.into(),
			protocol_name,
		}
		.into())
	}
}

#[derive(Deserialize, Serialize, PartialEq, Eq, Clone, Debug, Hash)]
pub struct MethodCall {
	pub protocol_name: String,
	pub method_name: String,
	#[serde(with = "serde_bytes")]
	pub message: Vec<u8>,
}

#[derive(Deserialize, Serialize, PartialEq, Eq, Clone, Debug, Hash)]
pub struct CallResponse {
	pub success: bool,
	#[serde(with = "serde_bytes")]
	pub message: Vec<u8>,
}

#[derive(Error, PartialEq, Eq, Clone, Hash, Debug)]
#[error("Protocol {} missing", protocol_name)]
pub struct ProtocolMissing {
	pub protocol_name: String,
}

#[derive(Error, Debug, PartialEq, Eq, Clone, Hash)]
#[error("Method {} on protocol {} missing", method_name, protocol_name)]
pub struct MethodMissing {
	pub method_name: String,
	pub protocol_name: String,
}

impl<State, ReadChannel: AsyncRead + Unpin, WriteChannel: AsyncWrite + Unpin> Responder<State, ReadChannel, WriteChannel> {
	pub fn add_protocol<ProtocolInstance: Protocol<State = State> + 'static>(&mut self, protocol: ProtocolInstance) {
		let protocol_name = protocol.name();
		self.protocols.insert(protocol_name, Box::new(protocol) as Box<dyn Protocol<State = State>>);
	}
	pub fn process_call(&mut self, method_call: &MethodCall) -> Result<CallResponse> {
		match self.protocols.get_mut(&method_call.protocol_name) {
			None => Err(ProtocolMissing {
				protocol_name: method_call.protocol_name.clone(),
			}
			.into()),
			Some(e) => e.method(&mut self.state, &method_call.method_name, &method_call.message),
		}
	}
	pub fn new(state: State, read_channel: ReadChannel, write_channel: WriteChannel, codec: Option<&tokio_util::codec::LengthDelimitedCodec>) -> Responder<State, ReadChannel, WriteChannel> {
		Responder {
			state,
			protocols: HashMap::new(),
			framed_duplex: FramedDuplex::new(read_channel, write_channel, codec),
		}
	}

	// TODO: Limits?
	pub fn answer_raw_call(&mut self, input: impl std::io::Read, mut output: impl std::io::Write) -> anyhow::Result<bool> {
		// TODO: add context to errors
		let method_call: MethodCall = ciborium::from_reader(input)?;
		let resp = self.process_call(&method_call)?;
		ciborium::into_writer(&resp, &mut output)?;
		output.flush()?;
		Ok(resp.success)
	}

	// TODO: batch mode with no recreating frameds, not sure if worth it
	pub async fn answer_call(&mut self) -> anyhow::Result<Option<bool>> {
		let next_inpacket = match self.framed_duplex.framed_reader.next().await {
			None => return Ok(None),
			Some(a) => a,
		}?;
		let mut outpacket = vec![];
		let result = self.answer_raw_call(next_inpacket.as_ref(), &mut outpacket);
		let result = result?;
		self.framed_duplex.framed_writer.send(outpacket.into()).await?;
		Ok(Some(result))
	}
}

pub struct Client<ReadChannel: AsyncRead + Unpin, WriteChannel: AsyncWrite + Unpin> {
	pub framed_duplex: FramedDuplex<ReadChannel, WriteChannel>,
}

impl<ReadChannel: AsyncRead + Unpin, WriteChannel: AsyncWrite + Unpin> Client<ReadChannel, WriteChannel> {
	pub fn new(read_channel: ReadChannel, write_channel: WriteChannel, codec: Option<&tokio_util::codec::LengthDelimitedCodec>) -> Self {
		Client {
			framed_duplex: FramedDuplex::new(read_channel, write_channel, codec),
		}
	}
	pub async fn call(&mut self, mc: &MethodCall) -> anyhow::Result<CallResponse> {
		self.framed_duplex.framed_writer.send(ciborium::into_vec(mc)?.into()).await?;
		let next_inpacket = self
			.framed_duplex
			.framed_reader
			.next()
			.await
			.context("Reading packet failed")?
			.map_err(|_| anyhow!("Channel closed before answer"))?;
		let cr: CallResponse = ciborium::from_reader(next_inpacket.as_ref())?;
		Ok(cr)
	}
}
