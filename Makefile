all: fmt
fmt-install:
	RUSTC_BOOTSTRAP=1 CFG_RELEASE_CHANNEL=stable CFG_RELEASE=1.4.18 cargo install --version 1.4.18 rustfmt-nightly
fmt:
	cargo fmt
