use anyhow::Result;

fn print_usage() {
	let brief = format!("test-client: make queries to CBORPC server on stdin/out, display output on stderr.\nUsage: test-client <protocol name> <method name> <message> ...",);
	eprint!("{}", &brief);
}

#[tokio::main]
async fn main() -> Result<()> {
	let args: Vec<_> = std::env::args().collect();
	if args.len() % 3 != 0 {
		print_usage();
		std::process::exit(10);
	}

	let mut client = cborpc::Client::new(tokio::io::stdin(), tokio::io::stdout(), None);

	let mut query_idx = 0;

	while query_idx < args.len() {
		let protocol_name = args[query_idx].to_string();
		let method_name = args[query_idx + 1].to_string();
		let message = &args[query_idx + 2]; // strings only
		eprintln!(">>> {}/{}({})", protocol_name, method_name, message);
		let response = client
			.call(&cborpc::MethodCall {
				protocol_name,
				method_name,
				message: ciborium::into_vec(message).unwrap(),
			})
			.await?;
		eprintln!("<<< success={} {:?}", response.success, response.message);
		eprintln!("???");
		query_idx += 3;
	}
	Ok(())
}
