use anyhow::Result;

struct Adder {
	acc: u8,
}

impl cborpc::Protocol for Adder {
	type State = u8;

	fn name(&self) -> String {
		"adder-0".into()
	}
	fn method(&mut self, state: &mut Self::State, method_name: &str, _message: &[u8]) -> Result<cborpc::CallResponse> {
		match method_name {
			"add" => {
				self.acc += 1;
				*state = self.acc + 1;
				Ok(cborpc::CallResponse {
					success: true,
					message: [self.acc, *state].into(),
				})
			}
			_ => self.method_missing(method_name),
		}
	}
}

#[tokio::main]
async fn main() -> Result<()> {
	let state = 0;

	let mut responder = cborpc::Responder::new(state, tokio::io::stdin(), tokio::io::stdout(), None);
	{
		responder.add_protocol(Adder { acc: 0 });
	}
	loop {
		match responder.answer_call().await {
			Ok(Some(success)) => {
				eprint!("Call answered, success={}", success);
				Ok(())
			}
			Ok(None) => Ok(()),
			Err(error) => Err(error),
		}?;
	}
}
